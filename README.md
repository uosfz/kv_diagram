# Visualize all possible implicants on a KV diagram

## Prerequisites
- Python 3
- numpy
- pillow

## Usage

```
python kvplot.py <number_of_variables>
```

The image will be saved in `output/kv_pixel_<number_of_variables>.png`

## Example Output
Black pixels are part of the implicant, white pixels aren't.

4 variables:

![](output/kv_pixel_4_s4.png)

5 variables:

![](output/kv_pixel_5_s4.png)

6 variables:

![](output/kv_pixel_6_s4.png)

7 variables:

![](output/kv_pixel_7_s2.png)

8 variables:

![](output/kv_pixel_8_s2.png)
