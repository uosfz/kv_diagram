from copy import deepcopy
import itertools
from typing import Iterator

# deepcopy (does not change old kv)
def increase_down(kv: list[list[int]], addl: int) -> list[list[int]]:
    new_kv = deepcopy(kv)
    for l in kv[::-1]:
        new_kv.append(list(elem + addl for elem in l))
    return new_kv

# deepcopy (does not change old kv)
def increase_side(kv: list[list[int]], addl: int) -> list[list[int]]:
    new_kv = deepcopy(kv)
    for l in new_kv:
        l += list(elem + addl for elem in l[::-1])
    return new_kv

def kv(num_vars: int) -> list[list[int]]:
    """Constructs a new kv diagram of the specified size.
    """
    if num_vars == 0:
        return [[0]]
    old = kv(num_vars - 1)
    addl = 1 << (num_vars - 1)
    return increase_side(old, addl) if num_vars % 2 != 0 else increase_down(old, addl)

def kv_dimensions(num_vars: int) -> (int, int):
    """Returns kv diagram size as (#rows, #columns) pair.
    """
    rows = 1 << (num_vars // 2)
    cols = 1 << ((num_vars + 1) // 2)
    return rows, cols

def kv_to_grid(index: int) -> (int, int):
    """Transforms kv index into (row, column) pair.
    """
    if index < 2:
        return 0, index
    shifted_index = index >> 2
    for n in itertools.count(2):
        if shifted_index == 0:
            index_offset = 1 << (n - 1)
            row, col = kv_to_grid(index - index_offset)
            value_offset = (1 << ((n + 1)//2)) - 1
            if n % 2 == 0:
                row = value_offset - row
            else:
                col = value_offset - col
            return row, col
        shifted_index >>= 1

def minterms_from_implicant(impl: list[str] | str) -> Iterator[list[bool]]:
    """Iterates over all minterms covered by an implicant.

    Minterms are yielded as list[bool].

    The implicant should be a string or list of "-" (don't care), "f" (false), or "t" (true) characters.

    Example: "-f-t" yields FFFT, FFTT, TFFT, TFTT
    """
    if len(impl) == 1:
        if impl[0] in "-f":
            yield [False]
        if impl[0] in "-t":
            yield [True]
    else:
        if impl[0] in "-f":
            for s in minterms_from_implicant(impl[1::]):
                yield [False] + s
        if impl[0] in "-t":
            for s in minterms_from_implicant(impl[1::]):
                yield [True] + s

def minterm_to_index(mt: list[bool]) -> int:
    """Transforms minterm into its MINt number.

    Example: [True,False,False] -> 4
    """
    res = 0
    for elem in mt:
        res = res * 2 + (1 if elem else 0)
    return res

