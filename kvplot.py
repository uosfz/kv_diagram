from PIL import Image
import numpy as np
import sys
import math
import itertools
import kv_helper

NUM_VARS = 4 # default value
try:
    NUM_VARS = int(sys.argv[1])
except (IndexError, ValueError):
    pass
print(f"{NUM_VARS=}")

num_plots = 3**NUM_VARS
num_plots_hori = num_plots_vert = math.ceil(num_plots**0.5)
kv_height, kv_width = kv_helper.kv_dimensions(NUM_VARS)

img_width = num_plots_hori * (kv_width + 3) + 1
img_height = num_plots_vert * (kv_height + 3) + 1
print(f"image dimensions: {img_width}x{img_height}")
a = np.full((img_height, img_width), 255, dtype='B')

gray = 200

x = 1 # x increases to the right (2nd numpy coordinate)
y = 1 # y increases downward (1st numpy coordinate)
for p in itertools.product("-ft", repeat=NUM_VARS):
    # draw border
    for xx in range(x, x + kv_width + 2):
        a[y, xx] = gray
        a[y + kv_height + 1, xx] = gray
    for yy in range(y, y + kv_height + 2):
        a[yy, x] = gray
        a[yy, x + kv_width + 1] = gray

    # draw kv implicant
    for minterm in kv_helper.minterms_from_implicant(p):
        index = kv_helper.minterm_to_index(minterm)
        row, col = kv_helper.kv_to_grid(index)
        a[y + 1 + row, x + 1 + col] = 0

    # advance
    x += kv_width + 3
    if x >= img_width:
        x = 1
        y += kv_height + 3

im = Image.fromarray(a, mode="L")
im.save(f"output/kv_pixel_{NUM_VARS}.png")
